# The configmount project

Config is a tool to mount configuration files into your filesystem, so that you can navigate through the configuration hierarchy and access its values like files.

## Warning

This is an early proof of concept version and not ready for production yet.

## Install

```
pip install configmount
```


## Example

Create a configuration directory for testing via `mkdir ~/etc` and place the following yaml file into it:

~/etc/config.yml
```yaml
group:
  key: value
  another group:
    key: value
```

Now create a mount directory `/mnt/config` and mount the configuration directory into it.

```
mkdir /mnt/config
configmount mount /mnt/config --dirs ~/etc
```

You should now be able to navigate into the group `/mnt/config/home/$USER/etc/config.yml/group` directory and
see the file `key` and the directory `another group`. Now you can change the configuration value of `key` like:

```
cd /mnt/config/home/$USER/etc/config.yml/group
ls
cat key
echo new value >key2
```

The changes are written back into the file as soon as you `echo save >"#interface"` within the directory `/mnt/config/home/$USER/etc/config.yml`.

Finally `cat ~/etc/config.yml` should give you the modified file.


To create a new configuration file do:

```
cd /mnt/config/home/$USER/etc
mkdir newfile.yml
cd newfile.yml
echo value >key
mkdir group
echo another value >group/key
echo save >"#interface"
```

The new configuration file is saved to:
```
cat /home/$USER/etc/newfile.yml
```

To umount the mountpoint do:

```
fusermount -uz /mnt/config
```

