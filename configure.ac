# Process this file with autoconf to produce a configure script.

# Copyright (C) 2017 Dominik Kummer <admin@arkades.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Arkades.org; either version 1, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


AC_PREREQ([2.69])
AC_INIT([configmount], [0.0.0], [admin@arkades.org])

VERSION=0.1.0

AC_PREFIX_DEFAULT([build])

AM_INIT_AUTOMAKE([-Wall -Werror -Wno-portability -Wno-override dist-xz filename-length-max=99 no-define no-dependencies])

AM_SILENT_RULES([yes])
AM_PATH_PYTHON([3.7])

AC_ARG_ENABLE([pkg],
    AS_HELP_STRING([--enable-pkg], [Enable package generation]),
    [GENPACKAGE="yes"])

# Keep this on a line of its own, since it must be found and processed
# by the 'update-copyright' rule in our Makefile.
RELEASE_YEAR=2019
AC_SUBST([RELEASE_YEAR], ["2019"])

# The API version is the base version.  We must guarantee
# compatibility for all releases with the same API version.
# Our current rule is that:
# * All releases, including the prereleases, in an X.Y series
#   are compatible.  So 1.5.1c is compatible with 1.5.
# * Prereleases on the trunk are all incompatible -- 1.5b and 1.5c
#   aren't the same.
APIVERSION=`echo "\$VERSION" | sed -e 's/^\([[0-9]]*\.[[0-9]]*[[a-z]]*\).*$/\1/'`

AC_SUBST([APIVERSION])
AC_SUBST([USER], ["domson"])
AC_SUBST([GROUP], ["\$(USER)"])
AC_SUBST([ADMINGROUP], ["\$(PACKAGE)"])
AC_SUBST([VPATH], ["\$(srcdir)"])
AC_SUBST([SHELL], ["/bin/bash"])
AC_SUBST([V], [0])

AC_SUBST([SUBDIRS])
AC_SUBST([STATICDIRS])
AC_SUBST([CLEANFILES])
AC_SUBST([DISTCLEANFILES])
AC_SUBST([MAINTAINERCLEANFILES])
AC_SUBST([EXTRA_DIST])
AC_SUBST([TAGS_FILES])
AC_SUBST([INSTALL], ["/usr/bin/install"])

AC_SUBST([pkgvdatadir], ["\${datadir}/$PACKAGE-$APIVERSION"])
AC_SUBST([scriptdir], ["\${pkgvdatadir}"])
AC_SUBST([pkgconfdir], ["\${sysconfdir}/$PACKAGE"])
AC_SUBST([pkgconf_DATA])

AC_SUBST([nobase_pkgconf_DATA])

AC_SUBST([dist_noinst_DATA])
AC_SUBST([nodist_noinst_DATA])
AC_SUBST([dist_noinst_SCRIPTS])
AC_SUBST([nodist_noinst_SCRIPTS])

AC_SUBST([bootdir], ["/boot"])

AC_SUBST([grubdir], ["\$(bootdir)/grub"])
AC_SUBST([grub_DATA])

AC_SUBST([lensdir], ["\$(pkgdatadir)/lenses"])
AC_SUBST([lens_DATA])

AC_SUBST([pkgdir], ["\$(pkgdatadir)"])
AC_SUBST([pkg_DATA])
AC_SUBST([nobase_pkg_DATA])

AC_SUBST([homedir], ["/home"])
AC_SUBST([home_DATA])
AC_SUBST([nobase_home_DATA])

AC_SUBST([pkghomedir], ["\$(homedir)/\$(USER)"])
AC_SUBST([pkghome_DATA])
AC_SUBST([nobase_pkghome_DATA])

AC_SUBST([bin_SCRIPTS])
AC_SUBST([rundir], ["/run"])

AC_SUBST([pkgstatedir], ["\$(rundir)/\$(PACKAGE)"])

AC_SUBST([memdir], ["/dev/shm"])
AC_SUBST([pkgmemdir], ["\$(memdir)/\$(PACKAGE)"])

AC_SUBST([logdir], ["\$(localstatedir)/log"])
AC_SUBST([pkglogdir], ["\$(logdir)/\$(PACKAGE)"])

AC_SUBST([sysconf_DATA])
AC_SUBST([nobase_sysconf_DATA])


AC_SUBST([kdeconfdir], ["\$(datadir)/config"])
AC_SUBST([kdeconf_DATA])

AC_SUBST([systemddir], ["\$(sysconfdir)/systemd/"])
AC_SUBST([systemdexecdir], ["\$(pkgdatadir)/systemd"])
AC_SUBST([pkglibdir], ["\$(libdir)/\$(PACKAGE)"])

if test -d "/usr/share/arkades/scripts"; then
	AC_SUBST([utilsdir], ["/usr/share/arkades/scripts"])
else
	AC_SUBST([utilsdir], ["\$(bindir)"])
fi

AC_SUBST([utils_SCRIPTS])

AC_SUBST([pkgbuilddir], ["\$(pkghomedir)/aur/\$(PACKAGE)"])
AC_SUBST([pkgbuild_DATA])

AC_SUBST([moduledir], ["\$(pkgdatadir)/modules"])
AC_SUBST([module_DATA])
AC_SUBST([nobase_module_DATA])

AC_SUBST([pymoddir], ["\$(pkgdatadir)/python"])
AC_SUBST([pymod_DATA])
AC_SUBST([nobase_pymod_DATA])

AC_SUBST([sharedir], ["\$(datadir)"])
AC_SUBST([share_SCRIPTS])
AC_SUBST([nobase_share_SCRIPTS])
AC_SUBST([share_DATA])
AC_SUBST([nobase_share_DATA])

AC_SUBST([templatesdir], ["\$(pkgdatadir)/templates"])

AC_DEFUN([NEXTVERSION], [[\$(shell if [ -f "\$(srcdir)/VERSION" ] && [[ \$\$(cat "\$(srcdir)/VERSION") =~ ^([0-9]+\.?)+$$ ]]; then cat "\$(srcdir)/VERSION"; else echo \$(VERSION); fi;)]])

AC_PROG_LN_S

AC_PATH_PROG([BASH], [bash])
if test -z "$BASH"; then
   AC_MSG_ERROR([bash not found])
fi

# Save details about the selected perl interpreter in config.log.
AM_RUN_LOG([$BASH --version])
AC_PROG_RANLIB
AC_PROG_MKDIR_P

## ---------------------- ##
##  Create output files.  ##
## ---------------------- ##

AC_CONFIG_FILES([Makefile])

AC_OUTPUT

# Inform the user if this version of automake is a beta release or
# a development snapshot.
# According to HACKING, the version of a development snapshot should
# end with an "odd" letter (a, c, ...), the version of a test release
# should end wit an "even" letter (b, d, ...).

am_stable_version_rx='[[1-9]\.[0-9]+(\.[0-9]+)?]'
am_beta_version_rx="[$am_stable_version_rx[bdfhjlnprtvxz]]"

am_release_type=`AS_ECHO(["\$PACKAGE_VERSION"]) | LC_ALL=C awk ["
  /^$am_stable_version_rx$/ { print \"stable\"; exit(0); }
  /^$am_beta_version_rx$/ { print \"beta version\"; exit(0); }
  { print \"development snapshot\"; }"]`

# '$silent' is set to yes if configure is passed the '--quiet' option.
test "$am_release_type" = stable || test "$silent" = yes || cat <<EOF

WARNING: You are about to use a $am_release_type of AC_PACKAGE_NAME.
WARNING: It might easily suffer from new bugs or regressions.
WARNING: You are strongly advised not to use it in production code.

Please report bugs, problems and feedback to <AC_PACKAGE_BUGREPORT>.
EOF

AS_EXIT([0])

